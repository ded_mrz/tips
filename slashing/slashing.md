TON Improvement Proposal for slashing condition specifications
==================================================

by Peter Fedorov. August 2020.

## Table of Contents
[TOC]

## Abstract
Slashing conditions are an integral part of almost all PoS blockchains. They are primarily intended to protect against problems like Nothing-at-Stake[6] that lead to forks and difficulty reaching consensus. In addition, slashing is often used to penalize validators who do not do their job well enough, and therefore harm the survivability of the network.  
TON (and FreeTON) originally designed penalties for unscrupulous validators, this is mentioned in the White Paper[1] and other documents[2]. The slashing code is partially implemented, but it is still incomplete and does not work automatically.

This document will look at the slashing ideas embedded in the TON system, the tools currently implemented, ways to improve them to cover all violations, proposals to change the economy of slashing, and a brief description of slashing in other blockchains.

## Punishment of validators according to the White Paper.
The TON documentation does not explicitly use the term slashing. Nevertheless, the possibility and necessity of fines and other punishments for unscrupulous validators has been repeatedly mentioned.

#### White Paper[1]
* 2.1.15: Validators and other nodes check the validity of the proposed block candidates; if a validator signs an invalid block candidate, it may be automatically punished by losing part or all of its stake, or by being suspended from the set of validators for some time.
* 2.1.16: Forks of the masterchain: We expect masterchain forks to be rare, next to non-existent, because in the BFT paradigm adopted by the TON Blockchain they can happen only in the case of incorrect behavior by a majority of validators (cf. 2.6.1 and 2.6.15), which would imply significant stake losses by the offenders.
* 2.1.17: Correcting invalid shardchain blocks:
    * Normally, only valid shardchain blocks will be committed, because validators assigned to the shardchain must reach a two-thirds Byzantine consensus before a new block can be committed. However, the system must allow for detection of previously committed invalid blocks and their correction.
    * Of course, once an invalid shardchain block is found—either by a validator (not necessarily assigned to this shardchain) or by a “fisherman” (any node of the system that made a certain deposit to be able to raise questions about block validity; cf. 2.6.4)—the invalidity claim and its proof are committed into the masterchain, and the validators that have signed the invalid block are punished by losing part of their stake and/or being temporarily suspended from the set of validators (the latter measure is important for the case of an attacker stealing the private signing keys of an otherwise benign validator).
* 2.6.1: Validators:
    * If a validator signs an invalid block, it can be punished by losing part or all of its stake, and by being temporarily or permanently excluded from the set of validators.
    * If a validator does not participate in creating a block, it does not receive its share of the reward associated with that block.
    * If a validator abstains from creating new blocks for a long time, it may lose part of its stake and be suspended or permanently excluded from the set of validators.
* 2.6.3: Nominators and “mining pools”:
    *  if the validator loses its stake because of invalid behavior, the nominator loses its share of the stake as well. In this sense the nominator shares the risk. It must choose its nominated validator wisely, otherwise it can lose money.
* 2.6.4: Fishermen: obtaining money by pointing out others’ mistakes.
    * Another way to obtain some rewards without being a validator is by becoming a fisherman. Essentially, any node can become a fisherman by making a small deposit in the masterchain. Then it can use special masterchain transactions to publish (Merkle) invalidity proofs of some (usually shardchain) blocks previously signed and published by validators. If other validators agree with this invalidity proof, the offending validators are punished (by losing part of their stake), and the fisherman obtains some reward (a fraction of coins confiscated from the offending validators). Afterwards, the invalid (shardchain) block must be corrected as outlined in 2.1.17. Correcting invalid masterchain blocks may involve creating “vertical” blocks on top of previously committed masterchain blocks (cf. 2.1.17); there is no need to create a fork of the masterchain.
    * Normally, a fisherman would need to become a full node for at least some shardchains, and spend some computing resources by running the code of at least some smart contracts. While a fisherman does not need to have as much computing power as a validator, we think that a natural candidate to become a fisherman is a would-be validator that is ready to process new blocks, but has not yet been elected as a validator (e.g., because of a failure to deposit a sufficiently large stake).
* 2.6.7: The stakes of the validators are frozen until the end of the period for which they have been elected, and one month more, in case new disputes arise (i.e., an invalid block signed by one of these validators is found).
* 2.6.11: All messages imported from other blockchains must be supported by suitable Merkle proofs in the collated data, otherwise the block candidate is deemed invalid (and, if a proof of this is committed to the masterchain, the validators having already signed this block candidate may be punished)
* 2.6.13: Validators must keep the blocks they have signed.
    * During their membership in the task group and for at least one hour (or rather 2¹⁰ blocks) afterward, the validators are expected to keep the blocks they have signed and committed. The failure to provide a signed block to other validators may be punished.
* 2.6.20: Slow validators may receive lower rewards.
    * If a validator is “slow”, it may fail to validate new block candidates, and two-thirds of the signatures required to commit the new block may be gathered without its participation. In this case, it will receive a lower share of the reward associated with this block.
    * This provides an incentive for the validators to optimize their hardware, software, and network connection in order to process user transactions as fast as possible.
    * However, if a validator fails to sign a block before it is committed, its signature may be included in one of the next blocks, and then a part of the reward (exponentially decreasing depending on how many blocks have been generated since—e.g., 0.9ᵏ if the validator is k blocks late) will be still given to this validator.
* 2.6.21: “Depth” of validator signatures.
    * Normally, when a validator signs a block, the signature testifies only to the relative validity of a block: this block is valid provided all previous blocks in this and other shardchains are valid. The validator cannot be punished for taking for granted invalid data committed into previous blocks.
    * However, the validator signature of a block has an integer parameter called “depth”. If it is non-zero, it means that the validator asserts the (relative) validity of the specified number of previous blocks as well. This is a way for “slow” or “temporarily offline” validators to catch up and sign some of the blocks that have been committed without their signatures. Then some part of the block reward will still be given to them (cf. 2.6.20).
* 2.6.22: Validators are responsible for relative validity of signed shardchain blocks; absolute validity follows.
    * In this way, the validator that signed block B cannot be punished if the original state s turns out to be “incorrect” (e.g., because of the invalidity of one of the previous blocks). A fisherman (cf. 2.6.4) should complain only if it finds a block that is relatively invalid.
    * In this way, the validator must perform minimal formal checks of the cells of the original state that are accessed during the evaluation
    * For example, imagine that the cell expected to contain the original balance of an account accessed from a transaction committed into a block turns out to have zero raw bytes instead of the expected 8 or 16. Then the original balance simply cannot be retrieved from the cell, and an “unhandled exception” happens while trying to process the block. In this case, the validator should not sign such a block on pain of being punished.
* 2.6.26. Relative reliability of a block.
    * if one of these validators loses part or all of its stake because of its misbehavior related to some other blocks, the relative reliability of a block may decrease.
* 2.6.27. “Strengthening” the blockchain.
    * It is important to provide incentives for validators to increase the relative reliability of blocks as much as possible. One way of doing this is by allocating a small reward to validators for adding signatures to blocks of other shardchains. Even “would-be” validators, who have deposited a stake insufficient to get into the top T validators by stake and to be included in the global set of validators (cf. 2.6.7), might participate in this activity (if they agree to keep their stake frozen instead of withdrawing it after having lost the election). Such would-be validators might double as fishermen (cf. 2.6.4): if they have to check the validity of certain blocks anyway, they might as well opt to report invalid blocks and collect the associated rewards.
* 2.6.28. Recursive reliability of a block.
    * we do not allow the validators to automatically reconsider blocks that are that old (i.e., created more than two months ago, if current values of configurable parameters are used), and create forks starting from them or correct them with the aid of “vertical blockchains” (cf. 2.1.17), even if they turn out to be invalid. We assume that a period of two months provides ample opportunities for detecting and reporting any invalid blocks, so that if a block is not challenged during this period, it is unlikely to be challenged at all.
* 2.7.4: Validator task groups for new shardchains:
    * Recall that each shard, i.e., each shardchain, normally is assigned a subset of validators (a validator task group) dedicated to creating and validating new blocks in the corresponding shardchain (cf. 2.6.8). These task groups are elected for some period of time (approximately one hour) and are known some time in advance (also approximately one hour), and are immutable during this period²³.
    * ²³Unless some validators are temporarily or permanently banned because of signing invalid blocks—then they are automatically excluded from all task groups.
* 2.8.3: if a validator is ever caught signing an incorrect block, and a proof of this is presented, part or all of its stake is forfeit.
* A.3: if a validator is caught misbehaving, a part or all of its stake will be taken away as a punishment, and a larger portion of it will subsequently be “burned”, decreasing the total supply of Grams. This would lead to deflation. A smaller portion of the fine may be redistributed to the validator or the “fisherman” who committed a proof of the guilty validator’s misbehavior
* A.4.5: stakes (validator deposits) are frozen for at least one month

#### Catchain[2]
* 2.7 Forks and their prevention
* 2.7.1. Detection of forks. The detection of forks is simple: if there are two different blocks m'i,s and m''i,s with the same creator i ∈ I and the same height s ≥ 1, and with valid signatures of i, then this is a fork.
* 2.7.2. Fork proofs. Block signatures in the Catchain protocol are created in such a way that creating fork proofs (i.e., the proof that a process i has intentionally created a fork) is especially simple since it is the hash of a very small structure (containing a magic number, the values of i and s, and the hash of the remainder of the message) that is actually signed. Therefore, only two such small structures and two signatures are required in a fork proof.
* 2.7.3. External punishment for creating forks. Notice that an external punishment for creating catchain forks may be used in the proof-of-stake blockchain generation context. Namely, the fork proofs may be submitted to a special smart contract (such as the elector smart contract of the TON Blockchain), checked automatically, and some part or all of the stake of the offending party may be confiscated.

#### Telegram Open Network Blockchain[3]
* 1.3.17. A validator can be punished for suggesting a collated block that does not contain all of the expected collated data inside, even if the block candidate itself is valid.

#### Summary on docs
Thus, penalties are provided for:

* Signing an invalid block candidate (penalty up to 100% of the stake + temporary or permanent exclusion).
* Participation in the fork of the network (penalty up to 100% of the stake).
* Signing an invalid shardchain block that was accepted by validators (up to 100% stake + temporary exclusion).
* Does not create blocks for long time (stake share penalty + temporary or permanent exclusion).
* Failure to provide a signed block from past (no punishment specified).
* Slow validators (decrease in block reward).
* Participation in the fork within the Catchain (penalty up to 100% of the stake).
* Suggestion of an incomplete collated block (no punishment specified).

Penalties for serious violations reach 100% of the stake. The second component of the penalty is temporary or permanent exclusion from the list of validators. This is done both to reduce the influence of an unscrupulous validator and to protect the validator from possible loss of the signature key. In addition, excluding a validator from the list of active ones protects the stake of its nominators (WP 2.6.3).
The amount received as a fine is supposed to be burned, thus reducing the amount of circulating Gram (TON Crystals), a small part of the fine can be transferred to the fisherman (WP A.3).

## Implemented tools
There are currently two main slashing tools.

1. The smart contract `elector-code.fc` has a code that allows anyone to provide evidence of a violation committed by one of the validators (register a complaint). The second part of the code allows validators to vote to penalize the violating validator if the evidence is valid, that is, to support the complaint. If 2/3 of the validators vote, then a fine is imposed on the violator, part of which is received by the one who presented the proof.
2. The `lite-client` implements three commands that allow you to find non-performing validators, create proofs of their low-quality work, and also check these proofs for validity.

Let's take a closer look at these tools.

#### Fisherman code algorithms
The fisherman code is in the `elector-code.fc` smart contract[4]. It allows you to create complaints, vote for complaints, and receive a list of active complaints, etc.

##### Registering a complaint
The message for registering a complaint must contain the payment for creating a complaint in the state, as well as a fee for registering it. The maximum reward depends on this fee. The message body consists of the following fields:

* `elections_id` election identifier.
* `complaint_obj` is a complaint object, consisting in turn of:
    * `validator_pubkey` public key of the accused validator.
    * `description` description (proof).
    * `created_at` creation time, will be overwritten by the contract.
    * `severity` is seriousness.
    * `reward_addr` address for payment of the reward, the contract substitutes the address from which the complaint was sent.
    * `paid` paid for complaint, contract substitutes real value.
    * `suggested_fine` suggested fine for the validator.
    * `suggested_fine_part` suggested stake fraction for the fine.

When the contract receives a message, the following happens:

1. The contract verifies that the complaint relates to an existing election that has not yet expired.
2. The fields `reward_addr`, ` created_at` and `paid` are filled. In this case, part of the tokens transferred in the message goes to pay for the creation and storage of the complaint, the rest is written in `paid`.
3. A new complaint object is created.
4. It is checked whether the accused validator is in the current list of validators.
5. It is checked that the penalty is not more than the frozen stake of the validator.
6. It is verified that the fine is greater than the paid for the complaint.
7. An additional object is created - the status of the complaint.
8. It is verified that such a complaint does not exist.
9. The complaint is added to the current election.
10. The state of the contract is saved.
11. The amount paid for creating a complaint is reserved (`raw_reserve`).
12. A return message is sent.

##### Vote for Complaint
The following arguments are accepted as input:

* `signature` The 512-bit signature of the following data must match the public key of the `idx` validator (see below).
* `sign_tag` 32-bit, must be equal to `0x56744350`.
* `idx` 16-bit index of the validator that votes for the penalty.
* `elect_id` 32-bit election identifier.
* `chash` 256-bit complaint identifier.

When the contract receives a message, the following happens:

1. The contract checks the value of `sign_tag`.
2. The weight and public key of the validator with the `idx` index are retrieved.
3. The correctness of the signature is checked.
4. It is checked that the election exists.
5. It is checked that the complaint exists.
6. It is checked that the complaint has not yet gained 2/3 of the total weight.
7. If the set of validators has changed, then the previous votes are discarded.
8. It is checked that the validator has not yet voted on the complaint.
9. The vote of the validator is added to the status of the complaint and the remaining weight is calculated.
10. If enough weight is gained, then the penalty is applied:
    1. It is checked that the punished validator has a frozen stake.
    2. The penalty is calculated equal to `suggested_fine + (stake * suggested_fine_part / 2 ^ 32)`, but not more than the size of the stake.
    3. A penalty is deducted from the frozen stake of the validator.
    4. The reward is calculated equal to the lesser of:
        1. fine / 8.
        2. (paid per complaint) * 8.
    5. The reward is sent to the address specified when creating the complaint.
    6. The difference between the penalty and the reward is added to the total funds of the contract (`grams`), and also subtracted from the ` total_stake` field, which stores the total stake of validators.
11. The state of the contract is saved.
12. A return message is sent.

##### Getting a raw list of complaints `get_past_complaints`
Accepts a election ID as an input.  
Returns a raw list of complaints in the specified vote.

##### Get list of complaints `list_complaints`
Accepts a election ID as an input.  
Returns a list of complaints unpacked including their hashes.

##### Details of `show_complaint` complaint
Accepts input:

* `election_id` election id.
* `chash` hash of the complaint.

Returns a complaint unpacked.

##### Get the cost of storing a complaint `complaint_storage_price`
Accepts input:

* `bits` number of bits to store.
* `refs` number of links to store.
* `expire_in` expiration date.

Returns a storage cost equal to `((bits + 1024) * bits_price + (refs + 2) * cell_price) * expire_in + deposit + 2 ^ 30`, where
(`deposit`, ` bit_price`,  `cell_price`) are taken from configuration parameter #13, by default they are: (` 2 ^ 36, 1, 512`).


#### Proof of poor performance generated by `lite-client`
In `lite-client`[5], three commands are implemented to control the quality of validators, create and check evidence:

* `checkloadall`, ` checkloadsevere` Checks whether all validators worked properly during specified time interval, and optionally saves proofs into `.boc`.
* `loadproofcheck` Checks a validator misbehavior proof previously created by `checkload`.

In the current implementation, the generated `.boc` file can be sent as evidence directly to the fisherman smart contract. Next, the validators extract the proof and check it using the `loadproofcheck` command. If the violation is confirmed, the validators vote to fine the culprit.

##### `checkload*`  work algorithm:
1. The input receives a time interval, for which the work of validators should be checked, the validity of the interval is checked, for example, that it is not in the future.
2. The headers of the two blocks of the master chain are retrieved, corresponding to the endpoints of the interval.
3. Configuration parameters #28 (Catchain settings) and #34 (list of validators) are retrieved corresponding to each of the received blocks.
4. It is checked that the sets of validators are the same.
5. Statistics are retrieved on the total number of blocks created (by each validator separately in the masterchain and shardchains) at the moments corresponding to the extreme points of the interval.
6. For each validator, the number of created blocks in the masterchain and shardchains for the specified period of time is calculated (the difference between the total number of blocks at the extreme points of the interval is calculated).
    1. The previously obtained values ​​from the configuration parameter #28 are also checked so that the number of masterchain and shardchains validators does not change.
7. Using the Monte Carlo method, the share of blocks in shardchains is calculated for each validator.
8. The basis is taken that the number of blocks produced by the validators corresponds to the Poisson distribution (which tends to the Normal distribution with an increase in the expectation value). For each validator, based on the number of blocks it produces, the probabilities are calculated (separately for the masterchain and shardchains) with which the number of blocks it can produce relative to 90% of the expected number of blocks. In other words, how likely is it that the number of blocks produced by the validator matches the allocation.
    1. If the probability is less than 0.001%, it is considered a severe violation.
    2. If the probability is less than 0.1%, it is considered a moderate violation.
    3. In both cases, a proof file is created if it was requested when the command was invoked.
    4. The proof also specifies the penalties for using in fisherman code (`validator_complaint`):
        1. For an interval less than 1000 seconds, no penalty is applied and no evidence file is generated.
        2. The base penalty and base stake share are determined by the severity of the violation:
            1. Severe = GR$2500, 1/4 stake.
            2. Moderate = GR$1000, 1/16 of a stake.
        3. If the interval is >= 80000 seconds, a base penalty is applied.
        4. Otherwise, if the interval is >= 20000 seconds, the base penalty is applied divided by 4.
        5. Otherwise, the base penalty is applied divided by 16.
        6. It should be noted that the generated proof contains a zero address for receiving a reward (`reward_addr`) and also a zero value of the ` paid` field, these values ​​will be filled by the smart contract upon receipt of a complaint.
9. Additionally, the results of the command are displayed on the screen.

The second command serves to check the evidence obtained from the outside.

##### `loadproofcheck` work algorithm:
1. A file with a proof comes in, an attempt is made to deserialize it.
2. The `ValidatorLoadInfo` objects corresponding to the endpoints of the interval are retrieved.
3. Similarly to the `checkload*` process, the lists of validators, the catchain config, the number of blocks created by each validator, etc. are retrieved. up to calculating the probabilities, the severity of the violation and the amount of fines.
4. The correspondence of the fine in the proof file and the calculated value is checked.
5. In case of inconsistencies, it is proposed to vote against the complaint, otherwise - for.


Thus, the code already has almost everything for slashing validators that do not fulfill their duties. It remains only to write scripts for monitoring violations and voting for fines, which will automate this.

## Needed improvements
For automatic slashing to work, we must:

1. Automatically find violations.
2. Automatically vote for violations found by other members.

To do this, it is proposed to write two scripts, each of which will do its job.

### Violation monitoring script algorithm:
1. The script is called every two minutes.
2. The script calls `checkloadsevere` and ` checkloadall` sequentially at intervals of 80000, 20000 and 1001 seconds.
    1. If the period overlaps with the change in the set of validators, the command will automatically end with an error, this will not be a problem for the script to work.
3. If no violations were found, the script ends.
4. If a violation is found, the most serious one found for each validator is selected. The severity is determined by the amount of the fine.
5. Next, for each of the violating validators, the list of previously saved violations is checked. In the list, the intersection of intervals is checked so as not to penalize for the same violation several times. At the same time, if the duration or severity of the violation has increased, the intersection of intervals is allowed, otherwise it will be impossible to fine for a more serious violation.
    1. It should be noted that the list is replenished not only with this script, but also with the script for voting for fines (see below).
6. If a suitable violation is found, a message is sent to the smart contract containing the proof of the violation.
7. If the submission is successful, the violation is saved in the processed list.

### Algorithm of the script voting for fines:
1. The script is called every minute.
2. The script gets a list of active complaints  `list_complaints`.
3. Each complaint is checked for its presence in the list of those already voted and in the list of those already excluded (dropout list).
4. If new complaints are found that are not in any list, then using the `lite-client` ` loadproofcheck` command, the validity of the proof is checked.
5. Invalid complaints are added to the dropout list.
6. All valid complaints are divided by validators and checked against the list of saved violations. Complaints with overlapping time intervals, which have not increased in severity, are added to the dropout list.
7. If at this stage there are several new overlapping complaints with the same severity, then one of them must be selected. It is selected according to the start time of the interval (lesser time). If the time is the same, the time at which the complaint was created (lesser time) is used. If the time is the same, then according to the hash (smaller hash). Those who are screened out are placed in the appropriate list.
8. If new valid complaints are found, the script should vote for them.
9. In case of successful voting, the complaint is placed in the list of voted ones, as well as in the list of violations (so that the first script does not send it again).

## Economy of current implementation
It should be noted that smaller (in terms of duration) disturbances accumulate before the onset of a longer one. That is, before a violation for 20000 seconds of downtime, the validator will receive 19-20 non-overlapping violations for 1001 seconds of downtime, and before a violation for 80000 seconds, it will receive 4 violations for 20000. At the same time, the amount of penalties:

* over 20000 is approximately equal to 16 offenses in 1001.
* over 80000 equals 4 offenses in 20000.

Thus, as soon as the violator crosses the threshold of the next violation, his total fine is approximately doubled.

Let's simulate the amount of penalties for a validator that does not create blocks for a long time, in this case 80000 seconds. The penalty consists of two parts: the absolute value and the share of the stake.

#### 1. Absolute values of penalties (orange = moderate, red = severe):
![chartGram.png](https://bitbucket.org/repo/MnjRRbb/images/1435043611-chartGram.png)

As you can see, the total penalty after 80000 seconds of downtime will be 4375 GRAM (TON Crystal), but if a certain proportion of blocks are produced (violation does not fall into the category of serious ones) the penalty will be 10 times less.

#### 2. Share of stake (orange = moderate, red = severe)

Explanation: The whole stake is 1.0 on the Y-axis, i.e. 50% = 0.5, 10% = 0.1.  
![chartStake.png](https://bitbucket.org/repo/MnjRRbb/images/2293911572-chartStake.png)

As you can see, if you do not produce blocks at all, then after 80,000 seconds (this is approximately 22 hours and a quarter), the validator will have slightly more than 16% of the stake. If you produce an insufficient number of blocks (moderate violation), then about 64% will remain.

That is, in less than a day, the validator will lose almost the entire stake. In my opinion, stake penalties are very high and should be reduced by a factor of 100 or more.


## Problems of the current implementation and how to solve them

#### Too big fines.
**Solution**: Reduce stake share penalties by 100 times.

#### There is no automatic generation of `.boc` to vote on a complaint
**Solution**: Add the required code to the `loadproofcheck` command. If the flag for generating a `.boc` file is specified, the command must create a file that can be sent directly to the smart contract.

#### Automation is performed by a separate script, if desired, it is easy to disable it.
**Solution**:

1. If most of the validators (2/3) are honest, then they will use the script, and it doesn't matter if the others disable the script. Moreover, it is possible to additionally penalize validators who do not systematically vote for penalties. To do this, you can collect statistics on the number of votes for fines and the number of signatures of validators. Statistics should be collected only when there are not enough votes to punish a poorly performing validator. If there are enough votes, the lack of signatures is not a problem. Some validators nodes may simply not have time to vote before 2/3, for example, because of the different local server time.
2. The logic of the scripts can be transferred directly to the node. In this case, it will be much more difficult to disable them.

#### It is impossible to flexibly change the amount of penalties, as well as the time intervals for which penalties are calculated.
**Solution**: we should  move the intervals and multipliers into the network configuration parameters so that the validators can change these values. For example `get_config (X)` will return:
```
{
  intervals: [1001, 20000, 80000],
  severities: {[
    {severity: 0.001, base_gram: 2500, base_part: 1/4, fines: [1/16, 1/4, 1]},
    {severity: 0.1, base_gram: 1000, base_part: 1/16, fines: [1/16, 1/4, 1]},
  ]},
}
```
Here, the fines correspond to the intervals.  
That is, all parameters are included in the config, and they can be changed. Moreover, you can change the number of intervals and the severities of violations.

#### Despite the penalties, the stability of the network is not improved by the fact that any validator stops participating in the consensus.
**Solution**: it is necessary to modify the smart contract so that after the maximum penalty (for 80000 seconds of inactivity) the validator is excluded from the list of active validators or marked inactive. At the same time, he stops receiving fines, because no longer interferes with finding consensus. An excluded validator can apply for inclusion in the active list after a certain time.  
The same logic is provided in WP 2.6.1

#### Only violations related to validator non-performing are taken into account.
**Solution**: we must also add evidence of other violations (see below).

#### The logic of the smart contract allows you to send overlapping and even coinciding violations, allowing you to penalize for one violation several times.
**Solution**: In automatic mode, when using the same scripts, this is not a problem for most honest validators. Only dishonest validators will send and vote overlapping complaints. Their weight will not reach 2/3.  
Nevertheless, for greater stability, we should move the logic of working with intervals into a smart contract so that it does not allow creating overlapping complaints.

#### At the moment, the guilty validators are not suspend (WP. 2.7.4)
**Solution**: it is necessary to modify the smart contract to suspend validators. Upon reaching the maximum duration of 80000 seconds, the validator should be removed from the list. In this case, it should be impossible to re-enable it earlier than a certain time specified in the configuration parameter.  
Other violations (double signing, participation in a fork, etc., see below) should also be brought to the immediate suspend of the validator.
It should be noted that the suspend of a validator should not change the list of validators that is used to check the quality of validators. It is necessary to leave the validator in the list, but mark it as inactive and prohibit any actions until the suspend expires.


## Other violations
The main focus of this document was to design slashing conditions for non-performing validators. Nevertheless, slashing is most often used for other violations, in particular, double signing (more details in the appendix A). Moreover, the white paper and other documents describe several other violations for which unscrupulous validators are supposed to be fined. For some of them, you can easily add automatic processing.

#### Signing Invalid Block Candidate (WP 2.1.15)
Formal proof of block invalidation is not currently supported (WP 2.2.4), so this violation can be added after the implementation of formal proof.  
Proposed 5% stake penalty and temporary suspend.

#### Participation in the fork of the network (WP 2.1.16)
Indicates the presence of validator signatures in more than one block that has the same height but different content. The formal proof of this violation is very simple, it is enough to provide the headers of two blocks that meet the criteria and the signatures of the accused validator.  
A proposed 100% stake fine and permanent suspend.

#### Signing an invalid shardchain block (WP 2.1.17)
The formal proof of the violation can be a vertical corrective block paired with a corrected block. Validators whose signatures are present in the corrected (invalid) block should be punished. It should be borne in mind that only the initial block to be corrected is a violation. Blocks that are corrected due to the correction of the first block are considered to be created properly.  
Proposed 25% stake penalty and temporary suspend.

#### Inability to provide own signed block from the past (WP 2.6.13)
Currently not supported in node implementation, formal proof is unknown. One of the proof options can be the absence of a validator's response to a public block request. That is, the accusing party, which was unable to receive a block previously signed by the validator, forms a request publicly on chain. The request has a time interval during which the validator must provide a block, otherwise its stake will be slashed. If the validator provides a block, then the public request is closed. But here we should take into account possible scenarios of abuse, so further research is needed.  
The proposed fine is 5% of the stake.

#### Fork to catchain (Catchain 2.7.3)
The Catchain document also mentions an additional violation at the consensus protocol layer (2.7 - 2.7.3). The violation is the same as participating in a fork of the network, the evidence is the same.  
Proposed 5% stake penalty and temporary suspend.

#### Summary
Thus, scripts that work with fisherman code can be easily improved to support two additional types of proofs: fork proof and vertical block proof. Alternatively, all supported self-contained proofs  can be verified in the smart contract.

The third type of proof is the lack of response to a request in the allotted time period, it should be worked out in more detail, the implementation in a smart contract will differ significantly.

It should also be noted that for these three types of evidence, it is not necessary for the validators to vote on the complaint in order to gain 2/3 of the weight. The contract can independently check their validity and fine the unscrupulous validator. But at the same time, we should add the processing of attempts to re-fine the culprit for the same violation.


## TIP Requirements
I will separately list the requirements of the contest for this document.

#### Be based on analysis of experience and attacks
1. The mechanisms of slashing in different blockchains were discussed in detail (see a summary in Appendix A).
2. The methods proposed in the White Paper were discussed in detail.

#### The honest and performing validator should not be slashed (criteria calculation for the honesty and performance should be provided)
For non-performing violations, numerical methods of mathematical analysis and probability theory are used. This leaves a fairly large margin of error. For an honest validator, the probability of receiving an unreasonable fine for a moderate violation is 0.1%, of receiving an unreasonable penalty for a serious violation of 0.001%.  
In other words, there is a chance of getting an unreasonable minimum penalty approximately every 1001 / (100/0.1) = 1001000 seconds or 3/4 years of continuous work. To obtain an unreasonable maximum fine, an average of 80,000 * (100 / 0.001) = 8 billion seconds, that is, more than 6,000 years, is needed.  
It should also be noted that these parameters can be changed.

#### Use existing Free TON Consensus rules
The available tools are used to the fullest extent, as well as the ideas described in the original TON documents.

#### Automate the slashing decision process on chain
The proposed slashing option uses helper scripts as the first step in implementation. It is also proposed to transfer part of the logic to a smart contract, and the logic for finding violators can be added directly to the node.

#### Automation for the slashing conditions for masterchain as well as shardchain should be proposed
Proofs of the quality of validators are collected simultaneously for both the masterchain and shardchains. In addition, the violations listed in the Other violations section also affect both masterchain and shardchains.

#### The description should be in clear technical language and sufficiently easy to understand
I hope the language and style used is clear and easy to understand. If the reader has any questions, do not hesitate, ask, and I will be happy to answer and clarify.

#### Need not to interfere with network performance
This implementation does not in any way increase the load on the network in any significant way. Detection of violations can be performed on a local node without using the network. Submitting complaints and voting for them affects the network no more than normal token transfer transactions between wallets.

#### Slashing economics should be discussed
A small analysis of the existing penalties for the lack of block production is presented. I propose to reduce the fines. For other violations, we should also think more carefully about the economy. I propose to make all the parameters customizable, this will allow us to flexibly respond to the experience gained during the network operation.

#### Slashing decision making process:
* **should be automated** - it's automated, no need to manual interaction.
* **must be based on objective criteria** - formal proofs and methods of mathematical analysis are used.
* **should not depend on any validator's desire or opportunity to vote** - the proposed scripts and changes to the smart contract become part of the validator software. If there are more than 2/3 of honest validators with original software, the desire and possibility of separate dishonest validators does not matter. Moreover, some proofs, such as double signing, vertical block, are self-sufficient, and the corresponding penalties can be applied without voting by other validators.

Thus, all requirements are met.

## Conclusion
The TON documentation (and therefore FreeTON) has a detailed description of the mechanisms for protecting the network from the actions of unscrupulous validators. These mechanisms have been partially implemented. To make the slashing mechanism fully operational, taking into account the experience of other blockchains, it is proposed to do the following:

* Implement scripts 1) monitoring violations and 2) voting for confirmed violations.
* Add to the smart contract of elections the ability to temporarily exclude validators from the list of active ones, including from all working groups.
* Revise downward stake penalty constants used in lite-client-generated proofs for non-performing validators.
* Add to lite-client the generation of `.boc` files for voting for complaints.
* The constants used to identify low-quality work, as well as to calculate the amount of penalties, should be transferred to the blockchain parameters so that validators can change them dynamically.
* Implement additional restrictions for overlapping complaints in the smart contract, so as not to punish for one violation several times. This only applies to complaints about non-performing.
* Implement automatic processing of double signing and vertical block proofs in a smart contract. This evidence is self-sufficient and does not require a vote on the complaint.
* Implement in the smart contract protection against re-sending double signing and vertical block complaints after the validator has already been punished.
* Make changes to the code of the node to automatically generate evidence double signing, double signing inside Catchain, creating a vertical block.
* For greater stability and determinism, transfer the logic of the proposed scripts directly to the node.
* Conduct research to create a formal proof of block invalidity (WP 2.1.15)
* Conduct more in-depth research into the evidence that the validator did not provide a signed block (WP 2.6.13). It is possible to implement the current proposed scheme.

The result will be a full-fledged slashing mechanism.



## Appndix A. Implementation of slashing in other blockchains
Several PoS blockchains have been investigated, in which one way or another it is necessary to solve similar problems. In most blockchains, slashing is used to counter Nothing-at-Stake, often validators are also penalized for downtime.
Slashing was one of the first to appear in Tezos [7], where it raised a lot of questions, and was partially modified in subsequent blockchains. For example, many of the following blockchains introduce the concept of jail [8] or similar to exclude the offending validator from the list of active ones. It is worth noting Polkadot, in which the concept of slashing is most fully worked out [9]. In particular, some of the terms TON (“fishermen” and “nominators”) were borrowed from Polkadot (WP 2.9.8). Standing apart is the Cardano blockchain, which has abandoned slashing. Its developers claim that due to more advanced protocols they were able to solve many problems that were previously solved using slashing [10].

The economics and some of the possibilities of slashing in different blockchains are summarized in the following table.

Column descriptions:

* blockchain - the name of the blockchain.
* downtime - penalty for downtime, "-" means no penalty.
* doubleSign - penalty for double signing.
* jail - whether a misbehaving validator is blocked.
* burn - whether the penalty or part of it is burned.
* reward - whether the reporter is rewarded.

blockchain|downtime|doubleSign|jail|burn|reward
-|-|-|-|-|-
**Cosmos**|0.01%|5%|+|+|+
**Polkadot**|0.1% and up|1% and up|+|-|+
**Near**|-|5%-100%|+|?|?
**Solana**|?|+|?|+|+
**Tezos**|-|512TZ|-|+|+
**Cardano**|-|-|-|-|-

More blockchains:

blockchain|downtime|doubleSign
-|-|-
Harmony |0.01%|2% and up
ICON    |6% | -
IRIS    |0.5% |1% and up
Livepeer|1%|3%
Terra   |0.01%|1%


## References
1. https://test.ton.org/ton.pdf
2. https://test.ton.org/catchain.pdf
3. https://test.ton.org/tblkch.pdf
4. https://github.com/ton-blockchain/ton/blob/master/crypto/smartcont/elector-code.fc
5. https://github.com/ton-blockchain/ton/blob/master/lite-client/lite-client.cpp
6. https://golden.com/wiki/Nothing-at-stake_problem
7. https://medium.com/cryptium/half-baked-is-always-better-than-double-baked-what-is-at-stake-in-the-tezos-protocol-6619ce4a5f87
8. https://hub.cosmos.network/master/validators/validator-faq.html#what-are-the-different-states-a-validator-can-be-in
9. https://w3f-research.readthedocs.io/en/latest/polkadot/slashing.html
10. https://medium.com/@undersearcher/how-secure-is-cardano-5f1e076be968

[1]: https://test.ton.org/ton.pdf
[2]: https://test.ton.org/catchain.pdf
[3]: https://test.ton.org/tblkch.pdf
[4]: https://github.com/ton-blockchain/ton/blob/master/crypto/smartcont/elector-code.fc
[5]: https://github.com/ton-blockchain/ton/blob/master/lite-client/lite-client.cpp
[6]: https://golden.com/wiki/Nothing-at-stake_problem
[7]: https://medium.com/cryptium/half-baked-is-always-better-than-double-baked-what-is-at-stake-in-the-tezos-protocol-6619ce4a5f87
[8]: https://hub.cosmos.network/master/validators/validator-faq.html#what-are-the-different-states-a-validator-can-be-in
[9]: https://w3f-research.readthedocs.io/en/latest/polkadot/slashing.html
[10]: https://medium.com/@undersearcher/how-secure-is-cardano-5f1e076be968


***

Peter Fedorov, August 2020  
0:1ee972c0d84c6138fd4d10fa70489907c596c2c91f4361e359beabc94078bf06
